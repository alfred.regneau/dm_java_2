import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

  /**
   * Ajoute deux entiers
   * @param a le premier entier à ajouter
   * @param b le deuxieme entier à ajouter
   * @return la somme des deux entiers
   */
  public static Integer plus(Integer a, Integer b){
      return a+b;
  }


  /**
   * Renvoie la valeur du plus petit élément d'une liste d'entiers
   * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
   * @param liste
   * @return le plus petit élément de liste
   */
  public static Integer min(List<Integer> liste){
    Integer min = null;
    for(int elem:liste)
      if(min == null || elem <= min)
        min = elem;
    return min;
  }


  /**
   * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
   * @param valeur
   * @param liste
   * @return true si tous les elements de liste sont plus grands que valeur.
   */
  public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    for(T elem:liste)
      if(valeur.compareTo(elem) >= 0)
        return false;
    return true;
  }



  /**
   * Intersection de deux listes données par ordre croissant.
   * @param liste1 une liste triée
   * @param liste2 une liste triée
   * @return une liste triée avec les éléments communs à liste1 et liste2
   */
  public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    Collections.sort(liste1);
    Collections.sort(liste2);
    ArrayList<T> res = new ArrayList<T>();
    int i = 0;
    int j = 0;
    while(i < liste1.size() && j < liste2.size()) {
      if (liste1.get(i).equals(liste2.get(j))){
        if(!res.contains(liste1.get(i)))
          res.add(liste1.get(i));
        i++;
        j++;
      }
      else if(liste1.get(i).equals(liste2.get(j))){
        if(!res.contains(liste1.get(i)))
          res.add(liste1.get(i));
        i++;
        j++;
      }
      else{
        if(liste1.get(i).compareTo(liste2.get(j)) < 0)
          i++;
        else
          j++;
      }
    }
    return res;
  }


  /**
   * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
   * @param texte une chaine de caractères
   * @return une liste de mots, correspondant aux mots de texte.
   */
  public static List<String> decoupe(String texte){
    List<String> res = new ArrayList<>();
    String values[]  = texte.split(" ");
    for (String e : values)
      if(!e.equals(""))
        res.add(e);
    return res;
  }


  /**
   * Renvoie le mot le plus présent dans un texte.
   * @param texte une chaine de caractères
   * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
   */

  public static String motMajoritaire(String texte){
    Map<String, Integer> dico = new HashMap<>();
    String res;
    for(String mot : decoupe(texte)){
      if(dico.containsKey(mot))
        dico.put(mot, dico.get(mot)+1);
      else
        dico.put(mot, 1);
    }
    if(dico.size() > 0){
      Map.Entry<String, Integer> maxEntry = null;
      for (Map.Entry<String, Integer> entry : dico.entrySet()) {
          if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) == 0 && entry.getKey().compareTo(maxEntry.getKey()) < 0)
            maxEntry = entry;
          if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
            maxEntry = entry;
      }
      res = maxEntry.getKey();
      return res;
    }
    else
      return null;
  }

  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de ( et de )
   * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
   */
  public static boolean bienParenthesee(String chaine){
    List<String> liste = new ArrayList<>();
    List<String> res = new ArrayList<>();
    String values[]  = chaine.split("");
    for (String e : values)
      if(e.equals("(") || e.equals(")"))
        liste.add(e);
    if(liste.size() >= 1){
      int nbParOuverte = 0;
      for(int i = 0; i < liste.size(); i++){
        if(liste.get(i).equals("("))
          nbParOuverte++;
        else if(liste.get(i).equals(")"))
          if(nbParOuverte >= 1)
            nbParOuverte--;
          else
            return false;
      }
      if(nbParOuverte == 0)
        return true;
      else
        return false;
    }
    return true;
  }

  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
   * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
   */
  public static boolean bienParentheseeCrochets(String chaine){
    List<String> liste = new ArrayList<>();
    String values[]  = chaine.split("");
    for (String e : values)
      if(e.equals("(") || e.equals(")") || e.equals("[") || e.equals("]"))
        liste.add(e);
    if(liste.size() >= 1){
      int nbParOuverte = 0;
      int nbCrochetOuvert = 0;

      // 1 = parenthese ouverte,  2 = crochet ouvert
      List<Integer> parCroOuverts = new ArrayList<>();

      for(int i = 0; i < liste.size(); i++){
        if(liste.get(i).equals("(")){
          nbParOuverte++;
          parCroOuverts.add(1);
        }else if(liste.get(i).equals("[")){
          nbCrochetOuvert++;
          parCroOuverts.add(2);
        }else if(liste.get(i).equals(")") && nbParOuverte >= 1 && parCroOuverts.size() >= 1){
          if(parCroOuverts.get(parCroOuverts.size()-1).equals(1)){
            nbParOuverte--;
            parCroOuverts.remove(parCroOuverts.size()-1);
          }
        }else if(liste.get(i).equals("]") && nbCrochetOuvert >= 1 && parCroOuverts.size() >= 1){
          if(parCroOuverts.get(parCroOuverts.size()-1).equals(2)){
            nbCrochetOuvert--;
            parCroOuverts.remove(parCroOuverts.size()-1);
          }
        }else
          return false;
      }
      if(nbParOuverte == 0 && nbCrochetOuvert == 0)
        return true;
      else
        return false;
    }
    return true;
  }


  /**
   * Recherche par dichtomie d'un élément dans une liste triée
   * @param liste, une liste triée d'entiers
   * @param valeur un entier
   * @return true si l'entier appartient à la liste.
   */
  public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    if(liste.size()<8)
      return liste.contains(valeur);
    else if(valeur < liste.get(0) || valeur > liste.get(liste.size()-1))
      return false;
    int bas = 0;
    int haut = liste.size();
    int milieu;
    while(haut-bas > 0){
      milieu = liste.get((int)bas+(haut-bas)/2);
      if(valeur > milieu)
        bas = milieu;
      else if(valeur < milieu)
        haut = milieu;
      else
        return true;
    }
    return false;
  }


}
